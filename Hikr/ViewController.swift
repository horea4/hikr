//
//  ViewController.swift
//  Hikr
//
//  Created by Horea Burca on 7/8/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var startStopButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var walkedLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private var images = RealmModel.shared.allImages()
    /**
     Used for notifications from Realm about changes to the database.
     */
    private var updateToken: NotificationToken?
    private var walkedTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0)
        tableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
        tableView.rowHeight = 250
        tableView.delegate = self
        tableView.dataSource = self
        
        updateStartStopButton()
        updateStatusLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // update the table every time the database is updated
        updateToken = images.addNotificationBlock { _ in
            self.tableView.reloadData()
        }
        walkedTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { _ in
            self.walkedLabel.text = "current distance: \(LocationService.shared.currentWalkedDistance)m"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        updateToken?.stop()
        walkedTimer?.invalidate()
    }
    
    func updateStartStopButton() {
        startStopButton.setTitle(DefaultsHelper.shared.isRecording ? "Stop" : "Start", for: .normal)
    }
    func updateStatusLabel() {
        statusLabel.isHidden = !DefaultsHelper.shared.isRecording
        walkedLabel.isHidden = !DefaultsHelper.shared.isRecording
    }
    
    // MARK: - Actions
    
    @IBAction func startStopTapped(sender: UIButton) {
        if !DefaultsHelper.shared.isRecording && !LocationService.isAuthorized {
            showAlert("Hmmm...", message: "Well, you see, Hikr needs permission to use your location. It's what it does. It's what it needs! Without it... there really is no point.\n\nPlease enable Location > Always from the app settings.", actionText: "OK, let's do that") { _ in
                self.gotoAppSettings()
            }
            return
        }
        
        DefaultsHelper.shared.isRecording = !DefaultsHelper.shared.isRecording
        updateStartStopButton()
        updateStatusLabel()
        
        if DefaultsHelper.shared.isRecording { // if we started recording, reset all the previous data first
            RealmModel.shared.reset()
            LocationService.shared.start()
        } else {
            LocationService.shared.stop()
        }
    }

    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageTableViewCell
        let image = UIImage(contentsOfFile: documentsDirectory().appendingPathComponent("\(images[indexPath.row].uid).jpg").path)
        cell.flickrImageView.image = image
        return cell
    }
    
    

}


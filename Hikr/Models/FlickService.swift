//
//  FlickService.swift
//  Hikr
//
//  Created by Horea Burca on 7/8/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON

/**
 Service that handles all Flickr operations and calls methods in the Realm model.
 */
class FlickService {
    
    static let apiKey = "302ee92f7d124c256651e203e33498dd"
    static let secret = "300d0157a44ae0c0"
    
    static let shared = FlickService()
    
    /**
     Makes a call to the Flickr API requesting the first 50 images at the location received as a parameter.
     Calls the download(flickrPhoto:forLocation:) function to download the image and then write to Realm.
     
     TODO: go through the image results and choose the first one that isn't already in the database
     */
    func fetchPhotos(forLocation location: CLLocation) {
        let parameters = [
            "method": "flickr.photos.search",
            "api_key": FlickService.apiKey,
            "format": "json",
            "nojsoncallback": "1",
            "lat": "\(location.coordinate.latitude)",
            "lon": "\(location.coordinate.longitude)",
            "per_page": "100",
//            "geo_context": "2",
//            "has_geo": "1",
//            "radius": "0.1",
        ]
        
        Alamofire.request("https://api.flickr.com/services/rest/", method: .post, parameters: parameters).responseJSON { json in
            
            // Obviously, checks should be done here to make sure the data received is correct;
            // That includes any error messages from the API, and all the data that's force-unwrapped;
            // But I think for the purpose of this challenge, that's not really needed;
            
            let json = JSON(data: json.data!)
            let photosJSON = json["photos"]["photo"]
            if photosJSON.count == 0 {
                return
            }
            
            for (_,photo) in photosJSON {
                let flickrPhoto = FlickrPhoto(id: photo["id"].string!,
                                              farm: photo["farm"].int!,
                                              secret: photo["secret"].string!,
                                              server: photo["server"].string!,
                                              title: photo["title"].string!)
                if RealmModel.shared.hasFlickrImage(withURL: flickrPhoto.photoURL.absoluteString) {
                    continue
                }
                print("downloading https://farm\(flickrPhoto.farm).staticflickr.com/\(flickrPhoto.server)/\(flickrPhoto.id)_\(flickrPhoto.secret).jpg")
                self.download(flickrPhoto: flickrPhoto, forLocation: location)
                break
            }
        }
    }
    
    /**
     Downloads the image and then makes a call to the RealmModel to write the image data in the database.
     */
    private func download(flickrPhoto: FlickrPhoto, forLocation location: CLLocation) {
        Alamofire.request(flickrPhoto.photoURL, method: .get).responseData { response in
            RealmModel.shared.addImage(fromFlickPhoto: flickrPhoto, andImageData: response.data, atLocation: location)
        }
    }
}

/**
 Holds relevant information about the Flickr image as received from the API.
 */
struct FlickrPhoto {
    let id: String
    let farm: Int
    let secret: String
    let server: String
    let title: String
    
    /**
     Generated Flickr URL for the image formed from the other propertied for this struct.
     */
    var photoURL: URL {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg")!
    }
    
}

//
//  LocationService.swift
//  Hikr
//
//  Created by Horea Burca on 7/8/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import Foundation
import CoreLocation

/**
 Handles all Location Service operations.
 */
class LocationService: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationService()
    
    static var isAuthorized: Bool {
        get {
            return CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() == .authorizedAlways
        }
    }
    
    private var manager: CLLocationManager
    private var walkedDistance: Double = 0
    private var lastLocation: CLLocation?
    
    public var currentWalkedDistance: Int {
        get {
            return Int(floor(walkedDistance))
        }
    }
    
    override private init() {
        manager = CLLocationManager()
        
        super.init()
        
        print("location authorization is \(CLLocationManager.authorizationStatus())")
        manager.activityType = .fitness
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.allowsBackgroundLocationUpdates = true
        manager.pausesLocationUpdatesAutomatically = false
        
        manager.delegate = self
    }
    
    /**
     Requests authorization from the user if needed.
     */
    func requestAuthorization() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            manager.requestAlwaysAuthorization()
        }
    }
    
    /**
     Starts the process of getting location updates.
     */
    func start() {
        self.manager.startUpdatingLocation()
    }
    
    /**
     Stops the location updates.
     */
    func stop() {
        self.manager.stopUpdatingLocation()
    }
    
    /**
     Starts listening for the 'significant' location changes. Used for when the app is terminated to tell the system that the app needs to be awaken.
     */
    func startBackgroundUpdates() {
        self.manager.startMonitoringSignificantLocationChanges()
    }
    
    func stopBackgroundUpdates() {
        self.manager.stopMonitoringSignificantLocationChanges()
    }
    
    // MARK: - Delegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        guard location.horizontalAccuracy < 20 && location.horizontalAccuracy > 0 else { return }
        
        if lastLocation != nil {
            let distance = location.distance(from: lastLocation!)
            walkedDistance += distance
            if walkedDistance >= 100 {
                print("fetching photos for next location \(location) and walkedDistance of \(walkedDistance)")
                walkedDistance = 0
                FlickService.shared.fetchPhotos(forLocation: location)
            }
        } else {
            print("fetching photos for first location \(location)")
            FlickService.shared.fetchPhotos(forLocation: location)
        }
        lastLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        // for the purpose of this test, I didn't spend time thinking what should happens when location update would fail consistently for some reason.
        print("error updating locations")
        
    }
}

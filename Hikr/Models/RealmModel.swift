//
//  RealmModel.swift
//  Hikr
//
//  Created by Horea Burca on 7/8/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import Foundation
import RealmSwift
import CoreLocation
import SwiftyJSON

/**
 Realm object for the image that we need to display with information about the time and location. The unique ID is used to generate the filename of the image file saved in the Documents directory.
 */
final class ImageData: Object {
    dynamic var uid: String = UUID().uuidString
    dynamic var addedOn: NSDate = NSDate()
    dynamic var flickrURL: String?
    dynamic var latitude: Double = 0
    dynamic var longitude: Double = 0
    
    override class func primaryKey() -> String? {
        return "uid"
    }
    
}

/**
 Handles all operations in the Realm database. 
 */
class RealmModel {
    
    public static let shared = RealmModel()
    
    private var realm: Realm!
    
    private init() {
        realm = try! Realm()
    }
    
    /**
     Deletes all the saved image files and purges the database.
     */
    func reset() {
        let images = allImages()
        for image in images {
            let fileURL = documentsDirectory().appendingPathComponent("\(image.uid).jpg")
            try? FileManager.default.removeItem(atPath: fileURL.path)
        }
        deleteAllImages()
    }
    
    /**
     Adds the image info to the database and saves the image file in the Documents directory.
     */
    func addImage(fromFlickPhoto photo: FlickrPhoto, andImageData data: Data?, atLocation location: CLLocation, errorHandler: (() -> Void)? = nil) {
        guard let photoData = data else { return }
        
        let imageData = ImageData()
        let fileURL = documentsDirectory().appendingPathComponent("\(imageData.uid).jpg")
        do {
            try photoData.write(to: fileURL)
        } catch {
            print("could not save image")
            return
        }
        imageData.latitude = location.coordinate.latitude
        imageData.longitude = location.coordinate.longitude
        imageData.flickrURL = photo.photoURL.absoluteString
        
        try! realm.write {
            realm.add(imageData)
        }
    }
    
    func hasFlickrImage(withURL url: String) -> Bool {
        let results = realm.objects(ImageData.self).filter("flickrURL = '\(url)'")
        return results.count > 0
    }
    
    /**
     Returns the CLLocation for the last image added in the database.
     */
    func lastImageLocation() -> CLLocation? {
        if let imageData = realm.objects(ImageData.self).sorted(byKeyPath: "addedOn", ascending: false).last {
            return CLLocation(latitude: imageData.latitude, longitude: imageData.longitude)
        }
        return nil
    }
    
    /**
     Gets all the images from the database ordered descending by date (sooner first).
     */
    func allImages() -> Results<ImageData> {
        return realm.objects(ImageData.self).sorted(byKeyPath: "addedOn", ascending: false)
    }
    
    private func deleteAllImages() {
        try! realm.write {
            realm.deleteAll()
        }
    }
}

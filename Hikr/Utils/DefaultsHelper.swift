//
//  DefaultsHelper.swift
//  Hikr
//
//  Created by Horea Burca on 7/8/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import Foundation

class DefaultsHelper {
    
    static let shared = DefaultsHelper()
    
    var defaults = UserDefaults.standard
    
    init() {
        
    }
    
    private let isRecordingKey = "IsRecording"
    var isRecording: Bool {
        set { defaults.set(newValue, forKey: isRecordingKey) }
        get { return defaults.bool(forKey: isRecordingKey) }
    }
}

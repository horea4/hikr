//
//  Utils.swift
//  Hikr
//
//  Created by Horea Burca on 7/9/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import Foundation

func documentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

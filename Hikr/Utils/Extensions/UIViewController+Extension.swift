//
//  UIViewController+Extension.swift
//  Hikr
//
//  Created by Horea Burca on 7/9/17.
//  Copyright © 2017 Blinky. All rights reserved.
//

import Foundation

import UIKit

extension UIViewController {
    
    func showAlert(_ title: String, message: String, actionText: String, actionHandler: ((UIAlertAction) -> Void)? = nil) {
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: actionText, style: .cancel, handler: actionHandler)
        alertViewController.addAction(okAction)
        
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func gotoAppSettings() {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.openURL(settingsUrl)
        }
    }
}
